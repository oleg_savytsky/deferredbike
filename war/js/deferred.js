$(document).ready(function() {
	var $res = $("span");
	var $val = $(".progress");
	var val = function(url, step){
		var step = step || 1;
		var deferred = $.Deferred();
		var previous = 0;
		
		var result = $.ajax(url, {
			xhr: function(){
				var xhr = new window.XMLHttpRequest();
				//Download progress
				xhr.addEventListener("progress", function(evt){
					try {
					  if (evt.lengthComputable){
						var percentComplete = evt.loaded / evt.total*100;
						//Do something with download progress
						if ( percentComplete - previous >= step ){
							previous = Math.floor(percentComplete/step)*step;
							deferred.notify( percentComplete.toFixed(1) );
						}
					  } else {
						  var currentValue = +$val.attr("value") + 1;
						  if (currentValue >= 100){
							  currentValue = 0;
						  }
						  $val.attr("value", currentValue);
					  }
					} catch (e){
						deferred.reject(e);
					}
				}, false);
				return xhr;
			  },
			success : function(){
				$val.attr("value", "100");
				deferred.resolve();
			},
			error : function(){
				deferred.reject("connect error");
			}			
		});
		return deferred.promise();
	};
	
	$('#pressDownload').on('click', function (){
		var $this = $(this);
		$res.show().text("");
		$this.attr("disabled", "disabled");
		var getLink = $('#link').val();
		val(getLink, 0.3).then(
			function(){
				$res.text("Download success").fadeOut(3000, function(){
					$this.removeAttr("disabled");
				});
			},
			function(e){
				$res.addClass("error").text(e.message);
			},
			function (result){
				$val.attr("value", result);
				$res.text(parseInt(result)+"%");
			}
		);
	});
});